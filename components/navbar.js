export default function Navbar() {
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item" href="">
          <img src="./assets/logo.webp" height="28" alt="logo" />
        </a>

        <a
          role="button"
          className="navbar-burger"
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
          href=""
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navbarBasicExample" className="navbar-menu">
        <div className="navbar-start">
          <a className="navbar-item" href="">
            {' '}
            Home{' '}
          </a>

          <a className="navbar-item" href="">
            {' '}
            Portfolio{' '}
          </a>

          <div className="navbar-item has-dropdown is-hoverable">
            <a className="navbar-link" href="">
              {' '}
              More{' '}
            </a>

            <div className="navbar-dropdown">
              <a className="navbar-item" href="">
                {' '}
                About{' '}
              </a>
              <a className="navbar-item" href="">
                {' '}
                Contact{' '}
              </a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
